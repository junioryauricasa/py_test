<div class="main-content">
	<div class="breadcrumbs" id="breadcrumbs">
		<script type="text/javascript">try{ace.settings.check('navbar' , 'fixed')}catch(e){}</script>
		<ul class="breadcrumb">
			<li> <i class="icon-home home-icon"></i>
				<a href="#">Home</a>
			</li>
			<li class="active">Reporte</li>
		</ul>
	</div>
	<div class="page-content">
		<div class="page-header">
			<h1> <i class="icon-hand-right icon-animated-hand-pointer blue"></i>
				Realizar Filtro
			</h1>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
				<div class="row">
					<div class="col-xs-12">
						<div class="widget-box">
							<div class="widget-body">
								<div class="widget-main">
									<form method="post" id="CreatePDFForm" target="_blank" hidden>
										<input type="hidden" name="title" id="title"/>
	                                    <input type="hidden" name="data" id="data"/>
	                                    <input type="hidden" name="detalle" id="detalle"/>
	                                </form>
									<form class="form-horizontal" role="form" id="frm-registro" name="frm-registro" action-1="<?php echo base_url();?>reporte/registrar" action-2="<?php echo base_url();?>reporte/actualizar">
										<div class="row">
											<div class="col-xs-12">
												<div class="col-xs-4">
													<div class="form-group">
														<label class="col-lg-2 control-label no-padding-right" for="fechaactual-reporte">Fecha</label>
														<div class="col-lg-8">
															<div class="input-group">
																<input type="text" id="fechaactual-reporte" name="fechaactual-reporte" class="form-control date-picker-edad validate[required,custom[date]]" class="col-xs-10 col-sm-5" />
																<span class="input-group-addon">
																	<i class="glyphicon glyphicon-calendar bigger-110"></i>
																</span>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-2 control-label no-padding-right"for="mes-reporte">Mes</label>
														<div class="col-lg-8">
															<select id="mes-reporte" name="mes-reporte" class="form-control validate[required]">
																<option value="-1">Seleccionar Mes</option>
																<option value="1">Enero</option>
																<option value="2">Febrero</option>
																<option value="3">Marzo</option>
																<option value="4">Abril</option>
																<option value="5">Mayo</option>
																<option value="6">Junio</option>
																<option value="7">Julio</option>
																<option value="8">Agosto</option>
																<option value="9">Septiembre</option>
																<option value="10">Octubre</option>
																<option value="11">Noviembre</option>
																<option value="12">Diciembre</option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-xs-4">
													<div class="form-group">
														<label class="control-label col-lg-2 no-padding-right"for="planta-reporte">Planta</label>
														<div class="col-lg-8">
															<select id="planta-reporte" name="planta-reporte" class="form-control validate[required]">
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label no-padding-right" for="anio-reporte">Año</label>
														<div class="col-sm-8">
															<input class="form-control validate[minSize[0],maxSize[4]" type="text" id="anio-reporte" name="anio-reporte" placeholder="Ej: 2016" maxlength="4" />
														</div>
													</div>
												</div>
												<div class="col-xs-4">
													<div class="form-group">
														<label class="control-label col-lg-2 no-padding-right"for="area-reporte">Area</label>
														<div class="col-lg-8">
															<select id="area-reporte" name="area-reporte" class="form-control validate[required]">
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-lg-2 no-padding-right"for="estado-reporte">Estado</label>
														<div class="col-lg-8">
															<select id="estado-reporte" name="estado-reporte" class="form-control validate[required]">
																<option value="-1">Todos</option>
																<option value="Pendiente">Pendiente</option>
																<option value="En Proceso">En Proceso</option>
																<option value="Finalizada">Finalizada</option>
															</select>
														</div>
													</div>
												</div>	
												<div class="col-xs-4">
													<div class="form-group">
														<label class="control-label col-lg-2 no-padding-right"></label>
														<div class="col-lg-8">
															<button id="btn-generar-reporte" class="btn btn-success" type="button">
																<i class="glyphicon glyphicon-search bigger-110"></i> Generar Reporte
															</button>
														</div>
													</div>
												</div>										
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>