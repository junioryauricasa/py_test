<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=lista_clientes.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table>
  <tr>
  	<td style="font-weight: bold;text-align: center; background-color:#BA5A41; color: #fff;" colspan="10" >LISTA DE USUARIOS</td>
  </tr>
  <tr>
  	<td colspan="10" ></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td style="font-weight: bold;">Fecha:</td>
    <td><?php echo date("Y-m-d H:i:s")?> </td>
  </tr>
  <tr>
  	<td colspan="10" ></td>
  </tr>
</table>
<table border="1">
	<thead>
		<tr>
			<th colspan="1" style="text-align: center;">Username</th>
			<th colspan="1" >Empleado &nbsp;</th>
			<th colspan="1" style="text-align: center;">Tipo </th>
			<th colspan="1" style="text-align: center;">Fecha creación</th>
		</tr>
	</thead>
	<tbody>
		<?php if(count($lstUsuarios)>0):?>
		<?php foreach ($lstUsuarios as $u):?>
		 	<tr>
		  		<td colspan="1" style="text-align: center;"><?php echo $u['username'];?></td>
		  		<td colspan="1"><?php echo $u['nom_empleado'];?></td>
		  		<td colspan="1" style="text-align: center;"><?php echo $u['type'];?></td>
		  		<td colspan="1" style="text-align: center;"><?php echo $u['fecharegistro'];?></td>
			</tr>
		  <?php endforeach;?>
		  <?php else :?>
		  <?php endif;?>
	</tbody>
</table>
