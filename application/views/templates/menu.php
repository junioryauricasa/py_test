		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>
			
			<div id="sidebar" class="sidebar responsive">
			
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<ul class="nav nav-list" id="MenuLateral">

					<li class="hsub" id="inicioprincipal">
						<a href="<?php echo base_url();?>principal">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> MENU PRINCIPAL </span>
						</a>
						<b class="arrow"></b>
					</li>

					<?php $lista_menu = $this->session->userdata('perfiles');
						if(!empty($lista_menu)){
							foreach ($lista_menu as $key => $row) {
								switch ($row['menu']) {
									case 'Incidencias':  ?>
										<?php if($row['estado']=='1'):  ?>
											<li class="hsub" id="menu-incidencias">
												<a href="<?php echo base_url();?>views/incidencias" id="ModuloIncidencias">
													<i class="menu-icon fa fa-picture-o"></i>
													<span class="menu-text"> INCIDENCIAS </span>
												</a>

												<b class="arrow"></b>
											</li>
										<?php endif?>
									<?php	break;
									 case 'Empleado': ?>
									 	<?php if($row['estado']=='1'):  ?>
											<li class="hsub" id="menu-empleado">
												<a href="<?php echo base_url();?>views/empleado" id="ModuloEmpleado">
													<i class="menu-icon fa fa-pencil-square-o"></i>
													<span class="menu-text"> EMPLEADO </span>

												</a>

												<b class="arrow"></b>
											</li>
										<?php endif?>
									<?php	break;
									case 'Usuario': ?>
										<?php if($row['estado']=='1'):  ?>
											<li class="hsub" id="menu-usuario">
												<a href="<?php echo base_url();?>views/usuario" id="ModuloUsuario">
													<i class="menu-icon fa fa-pencil-square-o"></i>
													<span class="menu-text"> USUARIO </span>

												</a>

												<b class="arrow"></b>
											</li>
										<? endif?>
									<?php break;
									case 'Reporte': ?>
										<?php if($row['estado']=='1'):  ?>
											<li class="hsub" id="menu-reporte">
												<a href="<?php echo base_url();?>views/reporte" id="ModuloReporte">
													<i class="menu-icon fa fa-pencil-square-o"></i>
													<span class="menu-text"> REPORTE </span>

												</a>

												<b class="arrow"></b>
											</li>
										<?php endif?>
									<?php break;
									case 'Configuración': ?>
										<?php if($row['estado']=='1'):  ?>
											<li class="hsub" id="menu-configuracion">
												<a href="<?php echo base_url();?>views/configuracion" id="ModuloConfiguracion">
													<i class="menu-icon fa fa-desktop"></i>
													<span class="menu-text"> CONFIGURACIÓN </span>

												</a>

												<b class="arrow"></b>
											</li>
										<?php endif?>
									<?php break;
								}
							}	
						}
						
					?>
					
				</ul>
				
			</div>
		</div>