			<!--<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="red bolder">								
								<img alt="Logo" src="<?php echo base_url();?>assets/images/logo_dv.png"> </span>&copy; 2014
						</span>
						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
					</div>
				</div>
			</div>-->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo base_url();?>assets/js/jquery.min.js'>"+"<"+"/script>");
		</script>

		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url();?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

		<script src="<?php echo base_url();?>assets/js/jquery-ui.custom.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/chosen.jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/markdown/markdown.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/markdown/bootstrap-markdown.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.hotkeys.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/bootstrap-wysiwyg.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>

		<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.dataTables.bootstrap.js"></script>

		<script src="<?php echo base_url();?>assets/js/ace-elements.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/ace.min.js"></script>

		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				if( !fixed || ( ace.helper.mobile_view() || ace.helper.collapsible() ) ) {
					$sidebar.removeClass('hide-before');
					ace.helper.removeStyle(sidebar , 'margin-top')
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('hide-before');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('hide-before');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			
			
			});
		</script>

		<script src="<?php echo base_url();?>assets/js/ace/elements.onpage-help.js"></script>
		<script src="<?php echo base_url();?>assets/js/ace/ace.onpage-help.js"></script>
		<script src="<?php echo base_url();?>docs/assets/js/rainbow.js"></script>
		<script src="<?php echo base_url();?>docs/assets/js/language/generic.js"></script>
		<script src="<?php echo base_url();?>docs/assets/js/language/html.js"></script>
		<script src="<?php echo base_url();?>docs/assets/js/language/css.js"></script>
		<script src="<?php echo base_url();?>docs/assets/js/language/javascript.js"></script>
		
		<script src="<?php echo base_url();?>assets/js/util/functiongen.js"></script>
		<script src="<?php echo base_url();?>assets/js/util/datatables.actions.js"></script>
		<script src="<?php echo base_url();?>assets/js/util/datatable_plugins.js"></script>

		<script src="<?php echo base_url();?>assets/js/jqueryvalidation/languages/jquery.validationEngine-es.js"></script>
		<script src="<?php echo base_url();?>assets/js/jqueryvalidation/jquery.validationEngine.js"></script>

		<script src="<?php echo base_url();?>assets/js/jquery.blockUI.js"></script>

		<script src="<?php echo base_url();?>assets/js/date-time/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url();?>assets/js/date-time/locales/bootstrap-datepicker.es.js"></script>
		<script src="<?php echo base_url();?>assets/js/date-time/bootstrap-timepicker.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/date-time/moment-with-langs.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/date-time/daterangepicker.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.min.js"></script>

		<!-- JS para wizard-->
		<script src="<?php echo base_url();?>assets/js/fuelux/fuelux.wizard.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/additional-methods.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/bootbox.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.maskedinput.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/select2.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/util/jquery.numeric.js"></script>

		<script src="<?php echo base_url();?>assets/js/app.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#<?=$dropactive ?>').addClass('active');
				$('#<?=$active ?>').addClass('active');
				$('#<?=$subactive ?>').addClass('active');
			});
		</script>

		<script src="<?php echo $jsvista;?>"></script>
		<script type="text/javascript">
			var base_url = "<?php echo base_url();?>";
		</script>
			<script type="text/javascript">
			$(document).ready(function(){
			});
		</script>

	</body>
</html>