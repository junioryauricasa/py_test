<div class="main-content">
	<div class="breadcrumbs" id="breadcrumbs">
		<script type="text/javascript">try{ace.settings.check('navbar' , 'fixed')}catch(e){}</script>
		<ul class="breadcrumb">
			<li> <i class="icon-home home-icon"></i>
				<a href="#">Home</a>
			</li>
			<li class="active">Configuración</li>
		</ul>
	</div>
	<div class="page-content">
		<div class="page-header">
			<h1> <i class="icon-hand-right icon-animated-hand-pointer blue"></i>
				Mantenedor Configuración
			</h1>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
				<div class="row">
					<div class="col-xs-8">
						<div class="widget-box">
							<div class="widget-body">
								<div class="widget-main">
									<div class="row">
										<div class="col-xs-6">
											<button id="btn-add-planta" class="btn btn-sm btn-danger" type="button">Nuevo Planta</button>
											<form class="form-horizontal" role="form" id="frm-planta" name="frm-planta" action-1="<?php echo base_url();?>configuracion/registrar_planta" action-2="<?php echo base_url();?>configuracion/actualizar_planta" style="margin-top: 5%;">
												<div class="row">
													<div class="col-xs-12">
														<div class="form-group">
															<label class="col-sm-3 control-label no-padding-right" for="nombre-planta">Planta</label>
															<div class="col-sm-8">
																<input class="form-control validate[required,minSize[2],maxSize[30]" type="text" id="nombre-planta" name="nombre-planta" placeholder=" Nombre " />
																<input class="form-control hidden" type="text" id="id-planta" name="id-planta"/>
															</div>
														</div>
													</div>
												</div>
												<div class="clearfix form-actions">
													<div class="col-md-12">
														<button id="btn-cancelar-planta" class="btn" type="reset">
															<i class="ace-icon fa fa-undo bigger-110"></i>
															Cancelar
														</button>
														<button id="btn-guardar-planta" class="btn btn-primary" type="button">
															<i class="glyphicon glyphicon-save bigger-110"></i>
															Guardar
														</button>
														<button id="btn-actualizar-planta" class="btn btn-primary" type="button">
															<i class="glyphicon glyphicon-save bigger-110"></i>
															Actualizar
														</button>
													</div>
												</div>
											</form>
											<div class="row">
												<div class="col-xs-12">
													<div class="box-header">
														<h3 class="header smaller lighter blue">Lista Planta</h3>
													</div>
													<div class="box-body table-responsive">
														<table class="table table-striped table-bordered bootstrap-datatable datatable" id="planta_table" 
														data-source ="<?php echo base_url();?>servicios/get_planta_all/">
															<thead>
																<tr>
																	<th>#</th>
																	<th>Nombre</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-6">
											<button id="btn-add-area" class="btn btn-sm btn-danger" type="button">Nuevo Area</button>
											<form class="form-horizontal" role="form" id="frm-area" name="frm-area" action-1="<?php echo base_url();?>configuracion/registrar_area" action-2="<?php echo base_url();?>configuracion/actualizar_area" style="margin-top: 5%;">
												<div class="row">
													<div class="col-xs-12">
														<div class="form-group">
															<label class="control-label col-lg-3 no-padding-right"for="cboPlanta">Planta</label>
															<div class="col-lg-8">
																<select id="cboPlanta" name="cboPlanta" class="form-control validate[required]">
																</select>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-3 control-label no-padding-right" for="nombre-area">Area</label>
															<div class="col-sm-8">
																<input class="form-control validate[required,minSize[2],maxSize[30]" type="text" id="nombre-area" name="nombre-area" placeholder=" Nombre " />
																<input class="form-control  hidden" type="text" id="id-area" name="id-area"/>
															</div>
														</div>
													</div>
												</div>
												<div class="clearfix form-actions">
													<div class="col-md-12">
														<button id="btn-cancelar-area" class="btn" type="reset">
															<i class="ace-icon fa fa-undo bigger-110"></i>
															Cancelar
														</button>
														<button id="btn-guardar-area" class="btn btn-primary" type="button">
															<i class="glyphicon glyphicon-save bigger-110"></i>
															Guardar
														</button>
														<button id="btn-actualizar-area" class="btn btn-primary" type="button">
															<i class="glyphicon glyphicon-save bigger-110"></i>
															Actualizar
														</button>
													</div>
												</div>
											</form>
											<div class="row">
												<div class="col-xs-12">
													<div class="box-header">
														<h3 class="header smaller lighter blue">Lista Area</h3>
													</div>
													<div class="box-body table-responsive">
														<table class="table table-striped table-bordered bootstrap-datatable datatable" id="area_table" 
														data-source ="<?php echo base_url();?>servicios/get_area_all/">
															<thead>
																<tr>
																	<th>#</th>
																	<th>Planta</th>
																	<th>Nombre</th>
																</tr>
															</thead>
															<tbody></tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="widget-box">
							<div class="widget-body">
								<div class="widget-main">
									<button id="btn-add-cargo" class="btn btn-sm btn-danger" type="button">Nuevo Cargo</button>
									<form class="form-horizontal" role="form" id="frm-cargo" name="frm-cargo" action-1="<?php echo base_url();?>configuracion/registrar_cargo" action-2="<?php echo base_url();?>configuracion/actualizar_cargo" style="margin-top: 5%;">
										<div class="row">
											<div class="col-xs-12">
												<div class="form-group">
													<label class="col-sm-3 control-label no-padding-right" for="nombre-cargo">Cargo</label>
													<div class="col-sm-8">
														<input class="form-control validate[required,minSize[2],maxSize[30]" type="text" id="nombre-cargo" name="nombre-cargo" placeholder=" Nombre " />
														<input class="form-control hidden" type="text" id="id-cargo" name="id-cargo"/>
													</div>
												</div>
											</div>
										</div>
										<div class="clearfix form-actions">
											<div class="col-md-12">
												<button id="btn-cancelar-cargo" class="btn" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Cancelar
												</button>
												<button id="btn-guardar-cargo" class="btn btn-primary" type="button">
													<i class="glyphicon glyphicon-save bigger-110"></i>
													Guardar
												</button>
												<button id="btn-actualizar-cargo" class="btn btn-primary" type="button">
													<i class="glyphicon glyphicon-save bigger-110"></i>
													Actualizar
												</button>
											</div>
										</div>
									</form>
									<div class="row">
										<div class="col-xs-12">
											<div class="box-header">
												<h3 class="header smaller lighter blue">Lista Cargos</h3>
											</div>
											<div class="box-body table-responsive">
												<table class="table table-striped table-bordered bootstrap-datatable datatable" id="cargo_table" 
												data-source ="<?php echo base_url();?>servicios/get_cargo_all/">
													<thead>
														<tr>
															<th>#</th>
															<th>Nombre</th>
														</tr>
													</thead>
													<tbody></tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>