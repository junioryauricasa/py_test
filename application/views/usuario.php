<div class="main-content">
	<div class="breadcrumbs" id="breadcrumbs">
		<script type="text/javascript">try{ace.settings.check('navbar' , 'fixed')}catch(e){}</script>
		<ul class="breadcrumb">
			<li> <i class="icon-home home-icon"></i>
				<a href="#">Home</a>
			</li>
			<li class="active">Usuario</li>
		</ul>
	</div>
	<div class="page-content">
		<div class="page-header">
			<h1> <i class="icon-hand-right icon-animated-hand-pointer blue"></i>
				Mantenedor Usuario
			</h1>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
				<div class="row">
					<div class="col-xs-12">
						<div class="widget-box">
							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" role="form" id="frm-registro" name="frm-registro" action-1="<?php echo base_url();?>usuario/registrar"action-2="<?php echo base_url();?>usuario/actualizar">
											<div class="row">
												<div class="col-xs-12">
													<div class="col-xs-6">
														<div class="form-group">
															<label class="col-sm-4 control-label no-padding-right" for="username-usuario">Username</label>
															<div class="col-sm-8">
																<input type="text" class="form-control hidden" id="id-usuario" name="id-usuario">
																<input class="form-control validate[required,minSize[2],maxSize[30]" type="text" id="username-usuario" name="username-usuario" placeholder=" Username " />
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-4 control-label no-padding-right" for="password-usuario">Password</label>
															<div class="col-sm-8">
																<input class="form-control validate[required,minSize[6],maxSize[30]" type="password" id="password-usuario" name="password-usuario" placeholder=" ******* " />
															</div>
														</div>
													</div>
													<div class="col-xs-6">
														<div class="form-group">
															<label class="control-label col-lg-3 no-padding-right"for="empleado-usuario">Empleado</label>
															<div class="col-lg-8">
																<select id="empleado-usuario" name="empleado-usuario" class="form-control validate[required]">
																</select>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-lg-3 no-padding-right"for="type-usuario">Tipo</label>
															<div class="col-lg-8">
																<select id="type-usuario" name="type-usuario" class="form-control validate[required]">
																	<option value="1">HelpDesk</option>
																	<option value="2">Técnico</option>
																	<option value="3">Supervisor</option>
																	<option value="4">Empleado</option>
																	<option value="5">Administrador</option>
																</select>
															</div>
														</div>
													</div>
												</div>
											</div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-4 col-md-8">
												<button id="btn-cancelar-usuario" class="btn" type="reset"><i class="ace-icon fa fa-undo bigger-110"></i>
													Cancelar
												</button>
												<button id="btn-guardar-usuario" class="btn btn-primary" type="button"><i class="glyphicon glyphicon-save bigger-110"></i>
													Guardar
												</button>
												<button id="btn-actualizar-usuario" class="btn btn-primary" type="button"><i class="glyphicon glyphicon-save bigger-110"></i>Actualizar
												</button>
											</div>
										</div>
									</form>
									<div class="row">
										<div class="col-xs-12">
											<div class="box-header">
												<div class="col-sm-12">
													<h3 class="header smaller lighter blue">
														<div class="row">
															<span class="col-sm-8">
																Lista de Usuario
															</span><!-- /.col -->
															<span class="col-sm-4">
																<label class="pull-right inline">
																	<a href="<?php echo base_url();?>exportarExcel/toExcel_listaUsuarios"
																		class='btn btn-primary btn-sm'>
																		Exportar a Excel
																	</a>
																</label>
															</span><!-- /.col -->
														</div>
													</h3>
												</div>
											</div>
											<div class="box-body table-responsive">
												<table class="table table-striped table-bordered bootstrap-datatable datatable" id="usuario_table"
																	data-source ="<?php echo base_url();?>
													servicios/get_usuario_all">
													<thead>
														<tr>
															<th>Username</th>
															<th>Empleado</th>
															<th>Tipo</th>
															<th>Fecha Creación</th>
														</tr>
													</thead>
													<tbody></tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal fade" id="modal-perfil-usuario" >
			        <div class="modal-dialog">
			            <div class="modal-content">
			                <div class="modal-header">
			                    <h3 class="panel-title">Perfil de Acceso</h3>
			                </div>
			                <div class="modal-body">
			                    <div class="row">
			                        <div class="col-lg-12">
			                            <div class="box-body table-responsive">
			                                <table id="tbl_perfil" class="table table-striped table-bordered" cellspacing="0" width="100%">
												<thead>
													<tr>
														<th>Menu</th>
														<th>Estado</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="modal-footer">
			                    <a href="#" id="btn-cancelar-perfil" class="btn btn-default" data-dismiss="modal"><i class="ace-icon fa fa-undo bigger-110"></i> Salir</a>
			                    <button data-dismiss="modal" class="btn btn-primary" id="btn-guardar-perfil" type="button">Guardar</button>
			                </div>
			            </div>
			        </div>
			    </div>

			</div>
		</div>
	</div>
</div>
