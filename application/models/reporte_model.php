<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reporte_model extends CI_Model {
	
	function get_reporte_byparam($fecha,$area,$mes,$anio,$estado){

		$add ="";
		if($fecha==-1){

			if($area!=-1 || $mes!=-1 || $anio!=-1){
				$add ="where ";
				$add = $area==-1? $add: $add.' e.idarea = '.$area;
				if($mes!=-1){
					if($add != "where "){
						$add =	$add.' and MONTH(i.fecharegistro)= '.$mes;
					}else{
						$add =	$add.' MONTH(i.fecharegistro)= '.$mes;
					}
				}

				if($anio!=-1){
					if($add != "where "){
						$add =	$add.' and YEAR(i.fecharegistro)= '.$anio;
					}else{
						$add =	$add.' YEAR(i.fecharegistro)= '.$anio;
					}
				}
				
				if($estado!=-1){
					if($add != "where "){
						$add =	$add." and i.estado='".$estado."'";
					}else{
						$add =	$add." i.estado='".$estado."'";
					}
				}

			}

		}else{

			$add = "where date(i.fecharegistro)= date('".$fecha."')";
			$add = $area==-1? $add: $add.' and e.idarea = '.$area;
			$add = $estado==-1? $add: $add." and estado='".$estado."'";

		}

		$query = $this->db->query("select i.keycode, i.titulo, concat(e.nombre,e.apellidos) as usuario_creo,i.fecharegistro, i.estado from incidencia i inner join usuario u on u.idusuario = i.usuariocrea inner join empleado e on e.idempleado = i.idempleado ".$add);
        return $query->result_array();

	}
	
}