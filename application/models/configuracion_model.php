<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class configuracion_model extends CI_Model {

	
	function __construct() {
		parent::__construct();
	}

	function insert_planta($data){
		if ($this->db->insert('planta',$data)){
			return true;
		}else{
			return false;
		}
	}

	function insert_area($data){
		if ($this->db->insert('area',$data)){
			return true;
		}else{
			return false;
		}
	}

	function insert_cargo($data){
		if ($this->db->insert('cargo',$data)){
			return true;
		}else{
			return false;
		}
	}
	
	function update_planta($id,$data){
		$this->db->where('idplanta',$id);
		if ($this->db->update('planta',$data)){
			return true;
		}else{
			return false;
		}
	}

	function update_area($id,$data){
		$this->db->where('idarea',$id);
		if ($this->db->update('area',$data)){
			return true;
		}else{
			return false;
		}
	}

	function update_cargo($id,$data){
		$this->db->where('idcargo',$id);
		if ($this->db->update('cargo',$data)){
			return true;
		}else{
			return false;
		}
	}

	function get_planta_all(){
		$query = $this->db->query("SELECT * FROM planta");
        return $query->result_array();
	}
	
	function get_area_all(){
		$query = $this->db->query("select area.*,planta.nombre as nom_planta from area
inner join planta on planta.idplanta = area.idplanta and area.estado=1");
        return $query->result_array();
	}

	function get_area_all_byIdPlanta($idplanta){
		if($idplanta!=-1){
			$query = $this->db->query("select area.*,planta.nombre as nom_planta from area inner join planta on planta.idplanta = area.idplanta and area.estado=1 where area.idplanta=".$idplanta);
			return $query->result_array();
		}		
	}

	function get_cargo_all(){
		$query = $this->db->query("SELECT * FROM cargo");
        return $query->result_array();
	}
	
}