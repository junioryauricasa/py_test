<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class incidencia_model extends CI_Model {

	
	function __construct() {
		parent::__construct();
	}

	function insert($data){
		if ($this->db->insert('incidencia',$data)){
			return true;
		}else{
			return false;
		}
	}

	function update($id,$data){
		$this->db->where('idincidencia',$id);
		if ($this->db->update('incidencia',$data)){
			return true;
		}else{
			return false;
		}
	}
	
	function get_incidencia_all(){
		$query = $this->db->query("SELECT i.*, date(i.fecharegistro) as fechacreacion, CONCAT(e.nombre,' ',e.apellidos) as nom_empleado_crea ,CONCAT(e1.nombre,' ',e1.apellidos) as nom_empleado_solicita, 
			e1.keycode as keycode_empleado_solicita, CONCAT(e2.nombre,' ',e2.apellidos) as nom_empleado_asignado FROM incidencia i inner join usuario u on u.idusuario = i.usuariocrea inner join empleado e on e.idempleado = u.idempleado inner join empleado e1 on e1.idempleado = i.idempleado inner join empleado e2 on e2.idempleado = i.empleadoasignado");
        return $query->result_array();
	}

	function obtenerkeyCode(){
		$query = $this->db->query("SELECT ( case when CAST(inc.keycode as UNSIGNED)=0 then '000001' else convert( right(concat('000000',(ifnull(CAST( inc.keycode as UNSIGNED),0) + 1)), 6) using latin1) end) as keycode_id FROM incidencia inc order by inc.idincidencia desc limit 0,1");
		return $query->row();
	}
	
}