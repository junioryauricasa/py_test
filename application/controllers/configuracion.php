<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class configuracion extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		//if (!$this->ion_auth->logged_in())
		//redirect('home', 'refresh');
	}

	public function registrar_planta(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('configuracion_model','cmodel');

			$nombre = $form["nombre-planta"];
			$data = array('nombre' =>$nombre, 'estado' =>'1');
			
			if($this->cmodel->insert_planta($data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}; 

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

	public function registrar_area(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('configuracion_model','cmodel');

			$idplanta = $form["cboPlanta"];
			$nombre = $form["nombre-area"];
			$data = array('nombre' =>$nombre, 'idPlanta'=>$idplanta, 'estado' =>'1');
			
			if($this->cmodel->insert_area($data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}; 

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

	public function registrar_cargo(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('configuracion_model','cmodel');

			$nombre = $form["nombre-cargo"];
			$data = array('nombre' =>$nombre, 'estado' =>'1');
			
			if($this->cmodel->insert_cargo($data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}; 

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

	public function actualizar_planta(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('configuracion_model','cmodel');

			$id = $form["id-planta"];
			$nombre = $form["nombre-planta"];
			$data = array('nombre' =>$nombre);
			
			if($this->cmodel->update_planta($id,$data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}; 

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

	public function actualizar_area(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('configuracion_model','cmodel');

			$id = $form["id-area"];
			$idplanta = $form["cboPlanta"];
			$nombre = $form["nombre-area"];
			$data = array('nombre' =>$nombre, 'idPlanta' =>$idplanta);
			
			if($this->cmodel->update_area($id,$data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}; 

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

	public function actualizar_cargo(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('configuracion_model','cmodel');

			$id = $form["id-cargo"];
			$nombre = $form["nombre-cargo"];
			$data = array('nombre' =>$nombre);
			
			if($this->cmodel->update_cargo($id,$data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}; 

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

}