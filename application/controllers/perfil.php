<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class perfil extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if ($this->ion_auth->logged_in()){}
		else
			redirect('home', 'refresh');
	}
	public function index()
	{
		if($this->ion_auth->in_group_type(1))
		{
			$dataheader['title'] = 'Perfil - ';
			$this->load->view('templates/headers.php',$dataheader);		
			$this->load->view('templates/menu.php');
			$this->load->view('perfil');
			$datafooter['jsvista'] =  base_url().'assets/js/jsvistas/perfil.js';
			$datafooter['active'] = '';
			$datafooter['dropactive'] = '';
			$datafooter['subactive'] = '';
			$this->load->view('templates/footer.php',$datafooter);
		}
		else
			redirect('/', 'refresh');
	}
}