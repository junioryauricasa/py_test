<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class incidencia extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//if (!$this->ion_auth->logged_in())
		//redirect('home', 'refresh');
	}

	public function registrar(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('incidencia_model','imodel');

			$idempleado = $form["idempleado-incidencia"];
			$titulo = $form["titulo-incidencia"];
			$keycode = $form["keycode-incidencia"];
			$prioridad = $form["prioridad-incidencia"];
			$tiemporesolucion = ''; //$form["tiemporesolucion-incidencia"];

			$fechaincidencia = $form['fechaincidencia-incidencia']==""?date("d/m/Y"):$form['fechaincidencia-incidencia'] ;
			$fechaincidencia = date_create_from_format("d/m/Y",$fechaincidencia);
			$fechaincidencia = $fechaincidencia->format('Y-m-d');

			$asignado = $form["asignado-incidencia"];
			$tipo = $form["tipo-incidencia"];
			$estado = $form["estado-incidencia"];
			$descripcion = $form["descripcion-incidencia"];
			$empleadoasignado = $form["idempleado-asignado"];
			
			//debemos obtener el codigo del usuario, cuando este el proceso de login
			$data = array('keycode' =>$keycode, 
						  'titulo' =>$titulo,
						  'tipoprioridad' =>$prioridad,
						  'tiemporesolucion' =>$tiemporesolucion,
						  'fechaIncidencia' =>$fechaincidencia,
						  'asginadoa' =>$asignado,
						  'empleadoasignado' =>$empleadoasignado,
						  'tipo' =>$tipo,
						  'estado' =>$estado,
						  'descripcion' =>$descripcion,
						  'idempleado' =>$idempleado,
						  'usuariocrea' =>$this->session->userdata('persona')[0]["idusuario"]
						  );
			
			if($this->imodel->insert($data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			};

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		} 
	
		$return = json_encode($return);
		echo $return;
	}

	public function actualizar(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('incidencia_model','imodel');

			$id = $form["id-incidencia"];
			$idempleado = $form["idempleado-incidencia"];
			$titulo = $form["titulo-incidencia"];
			$keycode = $form["keycode-incidencia"];
			$prioridad = $form["prioridad-incidencia"];
			$tiemporesolucion = ''; //$form["tiemporesolucion-incidencia"];

			$fechaincidencia = $form['fechaincidencia-incidencia']==""?date("d/m/Y"):$form['fechaincidencia-incidencia'] ;
			$fechaincidencia = date_create_from_format("d/m/Y",$fechaincidencia);
			$fechaincidencia = $fechaincidencia->format('Y-m-d');

			$asignado = $form["asignado-incidencia"];
			$tipo = $form["tipo-incidencia"];
			$estado = $form["estado-incidencia"];
			$descripcion = $form["descripcion-incidencia"];
			$empleadoasignado = $form["idempleado-asignado"];

			
			$data = array('keycode' =>$keycode, 
						  'titulo' =>$titulo,
						  'tipoprioridad' =>$prioridad,
						  'tiemporesolucion' =>$tiemporesolucion,
						  'fecharegistro' =>$fechaincidencia,
						  'asginadoa' =>$asignado,
						  'empleadoasignado' =>$empleadoasignado,
						  'tipo' =>$tipo,
						  'estado' =>$estado,
						  'descripcion' =>$descripcion,
						  'idempleado' =>$idempleado,
						  'usuariocrea' =>$this->session->userdata('persona')[0]["idusuario"]
						  );
			
			if($this->imodel->update($id,$data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}; 

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		}
	
		$return = json_encode($return);
		echo $return;
	}

	public function cerrar(){

		$form = $this->input->post('formulario');
	
		if ($form!=null){

			$this->load->model('incidencia_model','imodel');
			$id = $form["id-incidencia"];
			
			$fechaFinalizacion = date("y-m-d-h-i-s");
			$data = array('fechaFinalizacion'=>$fechaFinalizacion);

			if($this->imodel->update($id,$data)){
				$return = array("responseCode"=>200, "datos"=>"ok");
			}else{
				$return = array("responseCode"=>400, "greeting"=>"Bad");
			}

		}
		else {
			$return = array("responseCode"=>400, "greeting"=>"Bad");
		}
	
		$return = json_encode($return);
		echo $return;
	}

	public function obtenerkeyCode(){
		$this->load->model('incidencia_model','imodel');
		$result = $this->imodel->obtenerkeyCode();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('aaData' => $result)));
	}

}