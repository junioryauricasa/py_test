$(document).ready(function(){
	moment.lang('es');
	$('.date-picker').datepicker({
		format: "dd/mm/yyyy",
	    language: "es",
	    autoclose: true,
	    orientation: "top auto",
	    todayHighlight: true

	});

	$('.date-picker-edad').datepicker({
		format: "dd/mm/yyyy",
	    language: "es",	   
	    autoclose: true,
	    orientation: "top auto",
	    todayHighlight: true,
	    endDate: '-0y'
	})

	$('.date-picker-prev-actual').datepicker({
		format: "dd/mm/yyyy",
	    language: "es",	   
	    autoclose: true,
	    orientation: "top auto",
	    todayHighlight: true,
	    endDate: '-0d'
	})

	$('.date-picker-post-actual').datepicker({
		format: "dd/mm/yyyy",
	    language: "es",	   
	    autoclose: true,
	    orientation: "top auto",
	    todayHighlight: true,
	    startDate: '+0d'
	})

	$('.hora').timepicker({
		minuteStep: 1,
		showSeconds: false,
		showMeridian: true,
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

});