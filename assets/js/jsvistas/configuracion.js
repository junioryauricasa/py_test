var plantaTables,areaTables,cargoTables;
var sform_planta=true,sform_area=true,sform_cargo=true;
var id_planta_selec=0;

$(document).ready(function(){

	$("#frm-planta").hide();
	$("#frm-area").hide();
	$("#frm-cargo").hide();
	$("#btn-actualizar-planta").hide();
	$("#btn-actualizar-area").hide();
	$("#btn-actualizar-cargo").hide();

	var successEdit = function(){
		$.unblockUI({
			onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Actualización correcta", 
					buttons: {
						success: {
						label: "OK!",
						className: "btn-success"
						}
					}
				});
			}
		});
		$("#frm-planta").hide();
		$("#frm-planta").reset();
		$("#frm-area").reset();
		$("#frm-cargo").reset();
		$("#btn-guardar-planta").show();
		$("#btn-guardar-area").show();
		$("#btn-guardar-cargo").show();
		$("#btn-actualizar-planta").hide();
		$("#btn-actualizar-area").hide();
		$("#btn-actualizar-cargo").hide();
		reloadTable();
	}

	var errorEdit = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Error al Actualizar información", 
					buttons: {
						success: {
							label: "OK!",
							className: "btn-danger",
						}
					}
				});
		  	}
        });
	}

	var successPlanta = function(){
		$.unblockUI({
			onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Registro correcto", 
					buttons: {
						success: {
						label: "OK!",
						className: "btn-success"
						}
					}
				});
			}			
		});	
		reloadTable();
		$("#frm-planta").reset();
	}

	var errorPlanta = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Error al Registrar", 
					buttons: {
						success: {
							label: "OK!",
							className: "btn-danger",
						}
					}
				});
		  	}
        });	
	}

	var successCargo = function(){
		$.unblockUI({
			onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Registro correcto", 
					buttons: {
						success: {
						label: "OK!",
						className: "btn-success"
						}
					}
				});
			}			
		});	
		reloadTable();
		$("#frm-cargo").reset();
	}

	var errorCargo = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Error al Registrar", 
					buttons: {
						success: {
							label: "OK!",
							className: "btn-danger",
						}
					}
				});
		  	}
        });	
	}

	$('#btn-add-planta').click(function(event) {
		event.preventDefault();
		if(sform_planta) {
			$("#frm-planta").show();
			sform_planta =false;
		}else {
			$("#frm-planta").hide();
			sform_planta =true;
		}
	});
	$('#btn-add-area').click(function(event) {
		event.preventDefault();
		if(sform_area) {
			$("#frm-area").show();
			sform_area =false;
		}else {
			$("#frm-area").hide();
			sform_area =true;
		}
	});
	$('#btn-add-cargo').click(function(event) {
		event.preventDefault();
		if(sform_cargo) {
			$("#frm-cargo").show();
			sform_cargo =false;
		}else {
			$("#frm-cargo").hide();
			sform_cargo =true;
		}
	});
	$('#btn-guardar-planta').click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-planta").attr("action-1"),{formulario:$("#frm-planta").serializeObject()}, successPlanta, errorPlanta);
			}
		});
	});

	$('#btn-guardar-area').click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-area").attr("action-1"),{formulario:$("#frm-area").serializeObject()}, successCargo, errorCargo);
			}
		});
	});

	$('#btn-guardar-cargo').click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-cargo").attr("action-1"),{formulario:$("#frm-cargo").serializeObject()}, successCargo, errorCargo);
			}
		});
	});

	$("#btn-actualizar-planta").click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-planta").attr("action-2"),{formulario:$("#frm-planta").serializeObject()}, successEdit, errorEdit);
			}
		});
	});
	$("#btn-actualizar-area").click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-area").attr("action-2"),{formulario:$("#frm-area").serializeObject()}, successEdit, errorEdit);
			}
		});
	});
	$("#btn-actualizar-cargo").click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-cargo").attr("action-2"),{formulario:$("#frm-cargo").serializeObject()}, successEdit, errorEdit);
			}
		});
	});

	$("#btn-cancelar-planta").click(function(event){
		event.preventDefault();	
		$("#frm-planta").reset();
		//location.reload();
	});

	$("#btn-cancelar-area").click(function(event){
		event.preventDefault();	
		$("#frm-area").reset();
		//location.reload();
	});

	$("#btn-cancelar-cargo").click(function(event){
		event.preventDefault();	
		$("#frm-cargo").reset();
		//location.reload();
	});

	/*Tabla Planta*/
	var plantaTA = new DTActions({
		'botones': [
			{
				'icon':'fa fa-edit',
				'tooltip':'editar',
				'clickfunction': function(nRow, aData, iDisplayIndex) {
					$("#id-planta").val(aData.idplanta);
					$("#nombre-planta").val(aData.nombre);
					$("#btn-guardar-planta").hide();
					$("#btn-actualizar-planta").show();
					$("#frm-planta").show();
					sform_planta=false;
				},
			},
			]
	});

	var PlantaOptions = {
		"aoColumns":[
			{ "mDataProp": "idplanta"},
			{ "mDataProp": "nombre"},
		],
		"sDom": 'T<"clear">lfrtip',
		"fnCreatedRow": plantaTA.RowCBFunction,
		"responsive": true,
		"language": {
			"paginate": {
			  "previous": '<i class="fa fa-angle-left"></i>',
			  "next": '<i class="fa fa-angle-right"></i>'
			}
		},
	};
	plantaTables = createDataTable2('planta_table',PlantaOptions);
	
	/*Tabla Area*/
	var areaTA = new DTActions({
		'botones': [
			{
				'icon':'fa fa-edit',
				'tooltip':'editar',
				'clickfunction': function(nRow, aData, iDisplayIndex) {
					$("#id-area").val(aData.idarea);
					$("#nombre-area").val(aData.nombre);
					$("#btn-guardar-area").hide();
					$("#btn-actualizar-area").show();
					$("#cboPlanta").val(aData.idPlanta);
					$("#frm-area").show();
					sform_area=false;
				},
			},
			]
	});

	var AreaOptions = {
		"aoColumns":[
			{ "mDataProp": "idarea"},
			{ "mDataProp": "nom_planta"},
			{ "mDataProp": "nombre"},
		],
		"sDom": 'T<"clear">lfrtip',
		"fnCreatedRow": areaTA.RowCBFunction,
		"responsive": true,
		"language": {
			"paginate": {
			  "previous": '<i class="fa fa-angle-left"></i>',
			  "next": '<i class="fa fa-angle-right"></i>'
			}
		},
	};
	areaTables = createDataTable2('area_table',AreaOptions);

	/*Tabla Cargo*/
	var cargoTA = new DTActions({
		'botones': [
			{
				'icon':'fa fa-edit',
				'tooltip':'editar',
				'clickfunction': function(nRow, aData, iDisplayIndex) {
					$("#id-cargo").val(aData.idcargo);
					$("#nombre-cargo").val(aData.nombre);
					$("#btn-guardar-cargo").hide();
					$("#btn-actualizar-cargo").show();
				},
			},
			]
	});

	var CargoOptions = {
		"aoColumns":[
			{ "mDataProp": "idcargo"},
			{ "mDataProp": "nombre"},
		],
		"sDom": 'T<"clear">lfrtip',
		"fnCreatedRow": cargoTA.RowCBFunction,
		"responsive": true,
		"language": {
			"paginate": {
			  "previous": '<i class="fa fa-angle-left"></i>',
			  "next": '<i class="fa fa-angle-right"></i>'
			}
		},
	};
	cargoTables = createDataTable2('cargo_table',CargoOptions);
	reloadTable();

});

function reloadTable(){
	plantaTables.fnReloadAjax(base_url+"servicios/get_planta_all/");
	cargoTables.fnReloadAjax(base_url+"servicios/get_cargo_all/");
	areaTables.fnReloadAjax(base_url+"servicios/get_area_all/");
	cargar_combo_planta();
}

function cargar_combo_planta(){
	$('#cboPlanta').text("");
	 $.ajax({
        type: "GET",
        url: base_url+"servicios/get_planta_all/",
        data: {},
        dataType: "json",
        success: function (data) {
        	$('#cboPlanta').append('<option value="">Seleccionar Planta</option>');
            $.each(data.aaData, function (key,value) {
                $('#cboPlanta').append('<option value="'+ value.idplanta+ '">'+value.nombre+'</option>');
            });
        }
    });
}