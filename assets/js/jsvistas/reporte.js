var id_planta_area=0;
$(document).ready(function(){

	$('#anio-reporte').numeric();

	$("#planta-reporte").change(function(event){
		event.preventDefault();	
		cargar_combo_area($("#planta-reporte").val());
	});

	$("#btn-generar-reporte").click(function(event){
		
		var fecha = $("#fechaactual-reporte").val()==""?"-1": new Date($("#fechaactual-reporte").datepicker("getDates"));
		var anio = $("#anio-reporte").val()==""?"-1":$("#anio-reporte").val();
		var mes = $("#mes-reporte").val();
		var area = $("#area-reporte").val();
		var estado = $("#estado-reporte").val();

		if(fecha !=-1){
			fecha = fechaFormatoSQL(fecha);
		}
		
		ver_documento(fecha,area,mes,anio,estado);

	});

	cargar_combo_planta();

});

function cargar_combo_planta(){
	 $('#planta-reporte').text("");
	 $.ajax({
        type: "GET",
        url: base_url+"servicios/get_planta_all/",
        data: {},
        dataType: "json",
        success: function (data) {
        	$('#planta-reporte').append('<option value="-1">Todos</option>');
            $.each(data.aaData, function (key,value) {
                $('#planta-reporte').append('<option value="'+ value.idplanta+ '">'+value.nombre+'</option>');
            });
            cargar_combo_area($("#planta-reporte").val());
        }
    });
}

function cargar_combo_area(idplanta){
	$('#area-reporte').text("");
	$('#area-reporte').append('<option value="-1">Todos</option>');
	if(idplanta!=-1){
		$.ajax({
			type: "POST",
			url: base_url+"servicios/get_area_all_byIdPlanta/",
			data: {idplanta:idplanta},
			dataType: "json",
			success: function (data) {
			    $.each(data.aaData, function (key,value) {
			        $('#area-reporte').append('<option value="'+ value.idarea+ '">'+value.nombre+'</option>');
			    });
			    if(id_planta_area!=0){
			    	$("#area-reporte").val(id_planta_area);
			    }
			}
		});
	}

}

function ver_documento(fecha,area,mes,anio,estado){
	
	var add ="";
	if(fecha==-1){
		if(area!=-1 || mes!=-1 || anio!=-1){
			add = area==-1 ? add: add + $("#planta-reporte option:selected").text() + " - " +$("#area-reporte option:selected").text();
			if(mes!=-1){
				if(add != ""){
					add =	add + ' / ' + $("#mes-reporte option:selected").text();
				}else{
					add =	add + $("#mes-reporte option:selected").text();
				}
			}

			if(anio!=-1){
				if(add != ""){
					add =	add+' / '+$("#anio-reporte").text();
				}else{
					add =	add+$("#anio-reporte").text();
				}
			}
			
			if(estado!=-1){
				if(add != ""){
					add =	add + " / "+ $("#estado-reporte option:selected").text();
				}else{
					add =	add + $("#estado-reporte option:selected").text();
				}
			}

		}

	}else{

		add = fecha;
		add = area==-1? add: add + ' / '+ $("#planta-reporte option:selected").text() + " - " +$("#area-reporte option:selected").text();
		add = estado==-1? add: add+ ' / '+ $("#estado-reporte option:selected").text();

	}
	
    var url = base_url +"assets/extensiones/reportes_pdf/formato_reporte.php";
    $.ajax({
        type: "GET",
        url: base_url+"servicios/get_reporte_byparam/"+fecha+"/"+area+"/"+mes+"/"+anio+"/"+estado,
        data: {},
        dataType: "json",
        success: function (data) {
        	console.log()
        	$("#title").val(add);
        	$("#data").val(JSON.stringify(data.aaData));
        	$("#detalle").val('');
		    $("#CreatePDFForm").attr("action",url);
		    $("#CreatePDFForm").submit();
        }
    });
}