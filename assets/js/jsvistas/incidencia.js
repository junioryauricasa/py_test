var incidenciaTables,empleadoAsignarTables;
var selectEmpleado = new Array();
$(document).ready(function(){

	$("#btn-actualizar-incidencia").hide();
	$("#btn-cerrar-incidencia").hide();
	$("#empleado-asignado").css("background-color","#f0f5f5");

	$("#btn-buscar-empleado-asignar").click(function(e){
		e.preventDefault();
		var tipo_asignacion = $("#asignado-incidencia").val();
		var codigo_asignacion=0;
		switch (tipo_asignacion){
			case 'HelpDesk': codigo_asignacion =1; break;
			case 'Técnico': codigo_asignacion=2; break;
			case 'Supervisor': codigo_asignacion=3; break;
		}
		empleadoAsignarTables.fnReloadAjax(base_url+"servicios/get_empleados_asignar_byTipoAsignacion/"+codigo_asignacion);
		$("#modal-personal-asignar").modal('show');
	});

	$("#btn-seleccionar-empleo").click(function(e) {
		e.preventDefault();
		$('#idempleado-asignado').val(selectEmpleado[0].idempleado);	
		$('#empleado-asignado').text(selectEmpleado[0].empleado);
		$('#empleado-asignado').css("background-color","#F4FA58");
	});

	$("#btn-cerrar-incidencia").click(function(e){
		e.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-registro").attr("action-3"),{
					formulario:$("#frm-registro").serializeObject()
				}, successCancelar, errorCerrar);
			}
		});
	});

	var successCancelar = function(){
		$.unblockUI({
			onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Incidencia cerrada satisfactoriamente", 
					buttons: {
						success: {
						label: "OK!",
						className: "btn-success"
						}
					}
				});
			}
		});
		$("#btn-guardar-incidencia").hide();
		$("#btn-actualizar-incidencia").hide();
		$("#btn-cerrar-incidencia").hide();
		reloadTable();
	}

	var errorCerrar = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Error al Cerrar Incidencia", 
					buttons: {
						success: {
							label: "OK!",
							className: "btn-danger",
						}
					}
				});
		  	}
        });
	}

	var successEdit = function(){
		$.unblockUI({
			onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Actualización correcta", 
					buttons: {
						success: {
						label: "OK!",
						className: "btn-success"
						}
					}
				});
			}
		});
		id_planta_area = 0;

		var estado = $("#estado-incidencia option:selected").val();
		if(estado == "Finalizada"){
			$("#btn-actualizar-incidencia").hide();
			$("#btn-cerrar-incidencia").show();
		}
		//$("#frm-registro").reset();
		//$("#btn-guardar-incidencia").show();
		//$("#btn-actualizar-incidencia").hide();
		//$("#btn-cerrar-incidencia").hide();
		reloadTable();
	}

	var errorEdit = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Error al Actualizar información", 
					buttons: {
						success: {
							label: "OK!",
							className: "btn-danger",
						}
					}
				});
		  	}
        });
	}

	var successincidencia = function(){
		$.unblockUI({
			onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Registro correcto", 
					buttons: {
						success: {
						label: "OK!",
						className: "btn-success"
						}
					}
				});
			}			
		});	
		reloadTable();
		$("#frm-registro").reset();
	}

	var errorincidencia = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Error al Registrar", 
					buttons: {
						success: {
							label: "OK!",
							className: "btn-danger",
						}
					}
				});
		  	}
        });	
	}

	$('#btn-buscar-empleado').click(function(event){
		event.preventDefault();
		obtenerEmpleado_byKeyCode($("#keycodeEmpleado-incidencia").val());
	})

	$('#btn-guardar-incidencia').click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-registro").attr("action-1"),{formulario:$("#frm-registro").serializeObject()}, successincidencia, errorincidencia);
			}
		});
	});

	$("#btn-actualizar-incidencia").click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-registro").attr("action-2"),{formulario:$("#frm-registro").serializeObject()}, successEdit, errorEdit);
			}
		});
	});

	$("#btn-cancelar-incidencia").click(function(event){
		event.preventDefault();	
		$("#frm-registro").reset();
		$('#idempleado-asignado').val();	
		$('#empleado-asignado').text("");

		$("#id-incidencia").val("");
		$("#id-incidencia").css("background-color","#fff");
		$("#keycode-incidencia").val("");
		$("#keycode-incidencia").css("background-color","#fff");
		$("#keycodeEmpleado-incidencia").val("");
		$("#keycodeEmpleado-incidencia").css("background-color","#fff");
		$("#nombreapeEmpleado-incidencia").text("");
		$("#nombreapeEmpleado-incidencia").css("background-color","#fff");
		$("#cargoEmpleado-incidencia").text("");
        $("#cargoEmpleado-incidencia").css("background-color","#fff");
		$("#cargoEmpleado-cargo").text("");
        $("#cargoEmpleado-cargo").css("background-color","#fff");

        obtenerkeyCode();
	});

	$("#planta-incidencia").change(function(event){
		event.preventDefault();	
		cargar_combo_area($("#planta-incidencia").val());
	});

	var incidenciaTA = new DTActions({
		'botones': [
			{
				'icon':'fa fa-edit',
				'tooltip':'editar',
				'clickfunction': function(nRow, aData, iDisplayIndex) {

					$("#id-incidencia").val(aData.idincidencia);
					$("#keycode-incidencia").val(aData.keycode);
					$("#keycodeEmpleado-incidencia").val(aData.keycode_empleado_solicita);
					$("#nombreapeEmpleado-incidencia").text(aData.nom_empleado_solicita);

					$("#titulo-incidencia").val(aData.titulo);
					$("#idempleado-incidencia").val(aData.idempleado);
					$("#prioridad-incidencia").val(aData.tipoprioridad.trim());
					$("#idempleado-asignado").val(aData.empleadoasignado);
					$("#empleado-asignado").text(aData.nom_empleado_asignado);

					$("#tiemporesolucion-incidencia").val(aData.tiemporesolucion);
					$("#asignado-incidencia").val(aData.asginadoa);
					if(aData.fechacreacion!=""){
						var from = aData.fechacreacion.substring(0,10);
						var from = from.split("-");
						$("#fechaincidencia-incidencia").val(from[2]+"/"+from[1]+"/"+from[0]);
					}
					$("#tipo-incidencia").val(aData.tipo.trim());
					$("#estado-incidencia").val(aData.estado);
					$("#descripcion-incidencia").val(aData.descripcion);
					
					$("#btn-guardar-incidencia").hide();

					if(aData.fechaFinalizacion!=null){
						
						$("#btn-cerrar-incidencia").hide();
						$("#btn-actualizar-incidencia").hide();

					}else{

						if(aData.estado == "Finalizada"){
							$("#btn-actualizar-incidencia").hide();
							$("#btn-cerrar-incidencia").show();
						}else{
							$("#btn-actualizar-incidencia").show();
							$("#btn-cerrar-incidencia").hide();
						}

					}
					
					$("#incidenciaregistro").addClass('active');
					$("#incidencia-registro").addClass('active');
					$("#incidencia-lista").removeClass('active');
					$("#incidencialista").removeClass('active');

					$('#btn-buscar-empleado').trigger( "click" );
				},
			},
			]
	});

	var incidenciaOptions = {
		"aoColumns":[
			{ "mDataProp": "keycode"},
			{ "mDataProp": "titulo"},
			{ "mDataProp": "nom_empleado_solicita"},
			{ "mDataProp": "tipoprioridad"},
			{ "mDataProp": "fechacreacion"},
			{ "mDataProp": "asginadoa"},
			{ "mDataProp": "nom_empleado_asignado"},
			{ "mDataProp": "tipo"},
			{ "mDataProp": "estado"},
			{ "mDataProp": "nom_empleado_crea"},
		],
		"sDom": 'T<"clear">lfrtip',
		"fnCreatedRow": incidenciaTA.RowCBFunction,
		"responsive": true,
		"language": {
			"paginate": {
			  "previous": '<i class="fa fa-angle-left"></i>',
			  "next": '<i class="fa fa-angle-right"></i>'
			}
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			console.log($(nRow));
			console.log(aData);
			switch(aData['estado']){
				case 'Finalizada':
					$(nRow).find('td').eq(2).css('background-color', '#FA5858');
					break;
			}
			return nRow;	        
		}
	};
	incidenciaTables = createDataTable2('incidencia_table',incidenciaOptions);

	var empleado_asignar_options = {
		"aoColumns":[
			{ "mDataProp": "idempleado"},
			{ "mDataProp": "empleado"},
			{ "mDataProp": "dni"},
		],
		"sDom": 'T<"clear">lfrtip',
		"fnCreatedRow":getSimpleSelectRowCallBack(selectEmpleado),
		"responsive": true,
		"language": {
			"paginate": {
			  "previous": '<i class="fa fa-angle-left"></i>',
			  "next": '<i class="fa fa-angle-right"></i>'
			}
		},
	};
	empleadoAsignarTables = createDataTable2('tabla-empleado-asignar',empleado_asignar_options);

	reloadTable();

});

function reloadTable(){
	incidenciaTables.fnReloadAjax(base_url+"servicios/get_incidencia_all/");
	obtenerkeyCode();
}

function obtenerkeyCode(){
	 $.ajax({
        type: "GET",
        url: base_url+"incidencia/obtenerkeyCode/",
        data: {},
        dataType: "json",
        success: function (data) {
        	$("#keycode-incidencia").val(data.aaData.keycode_id);
        }
    });
}

function obtenerEmpleado_byKeyCode(keycode_empleado){
	$("#nombreapeEmpleado-incidencia").css("background-color","#fff");
    $("#cargoEmpleado-incidencia").css("background-color","#fff");
    $("#cargoEmpleado-cargo").css("background-color","#fff");
	$.ajax({
        type: "POST",
        url: base_url+"servicios/obtenerEmpleado_keyCode/",
        data: {keycode_empleado:keycode_empleado},
        dataType: "json",
        success: function (data) {
        	$("#idempleado-incidencia").val(data.aaData.idempleado);
        	$("#nombreapeEmpleado-incidencia").text(data.aaData.nombre+" "+data.aaData.apellidos);
        	$("#nombreapeEmpleado-incidencia").css("background-color","#F4FA58");
        	$("#cargoEmpleado-incidencia").text(data.aaData.planta_area);
        	$("#cargoEmpleado-incidencia").css("background-color","#F4FA58");
        	$("#cargoEmpleado-cargo").text(data.aaData.nom_cargo);
        	$("#cargoEmpleado-cargo").css("background-color","#F4FA58");
        }
    });
}
