var usuarioTables;
var id_planta_area=0;
var id_usuarioreg=0;
$(document).ready(function(){

	$("#btn-actualizar-usuario").hide();

	$('#empleado-usuario').change(function(){
		$("#username-usuario").val($("#empleado-usuario option:selected")[0].dataset.email);
	});

	var successEdit = function(){
		$.unblockUI({
			onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Actualización correcta", 
					buttons: {
						success: {
						label: "OK!",
						className: "btn-success"
						}
					}
				});
			}
		});
		id_planta_area = 0;
		$("#frm-registro").reset();
		$("#btn-guardar-usuario").show();
		$("#btn-actualizar-usuario").hide();
		reloadTable();
	}

	var errorEdit = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Error al Actualizar información", 
					buttons: {
						success: {
							label: "OK!",
							className: "btn-danger",
						}
					}
				});
		  	}
        });
	}

	var successusuario = function(){
		$.unblockUI({
			onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Registro correcto", 
					buttons: {
						success: {
						label: "OK!",
						className: "btn-success"
						}
					}
				});
			}			
		});	
		reloadTable();
		$("#frm-registro").reset();
	}

	var errorusuario = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					title: "Notificación",
					message: "Error al Registrar", 
					buttons: {
						success: {
							label: "OK!",
							className: "btn-danger",
						}
					}
				});
		  	}
        });	
	}

	$('#btn-guardar-usuario').click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-registro").attr("action-1"),{formulario:$("#frm-registro").serializeObject()}, successusuario, errorusuario);
			}
		});
	});

	$("#btn-actualizar-usuario").click(function(event) {
		event.preventDefault();
		$.blockUI({
			onBlock: function(){
				enviar($("#frm-registro").attr("action-2"),{formulario:$("#frm-registro").serializeObject()}, successEdit, errorEdit);
			}
		});
	});

	$("#btn-cancelar-usuario").click(function(event){
		event.preventDefault();	
		id_planta_area = 0;
		$("#frm-registro").reset();
	});

	var usuarioTA = new DTActions({
		'botones': [
			{
				'icon':'fa fa-edit',
				'tooltip':'editar',
				'clickfunction': function(nRow, aData, iDisplayIndex) {
					$("#id-usuario").val(aData.idusuario);
					$("#username-usuario").val(aData.username);
					$("#empleado-usuario").val(aData.idempleado);
					$("#type-usuario").val(aData.type);
					$("#btn-guardar-usuario").hide();
					$("#btn-actualizar-usuario").show();
				},
			},
			{
				'icon':'fa fa-table',
				'tooltip':'Perfiles',
				'clickfunction': function(nRow, aData, iDisplayIndex) {
					id_usuarioreg = aData.idusuario;
					reloadPerfil(id_usuarioreg);
				},
			}
			]
	});

	var usuarioOptions = {
		"aoColumns":[
			{ "mDataProp": "username"},
			{ "mDataProp": "nom_empleado"},
			{ "mDataProp": "nom_type"},
			{ "mDataProp": "fecharegistro"},
		],
		"sDom": 'T<"clear">lfrtip',
		"fnCreatedRow": usuarioTA.RowCBFunction,
		"responsive": true,
		"language": {
			"paginate": {
			  "previous": '<i class="fa fa-angle-left"></i>',
			  "next": '<i class="fa fa-angle-right"></i>'
			}
		},
	};
	usuarioTables = createDataTable2('usuario_table',usuarioOptions);
	reloadTable();

	var arrayCheck = new Array();
	var accesosOptions = {
		"aoColumns":[
			{ "mDataProp": "menu"},
			{ "mDataProp": "check"}
		],
		"fnCreatedRow":function(nRow, aData, iDisplayIndex)
		{
			arrayCheck.push($(nRow).find(".cbox"));
			$(nRow).find(".cbox").change(function(){
				if($(this).is(':checked'))
				{
					$(nRow).find(".desc").text(" Con acceso");
					aData.estado = 1;
				}
				else
				{
					$(nRow).find(".desc").text(" Sin acceso");
					aData.estado = 0;
				}
			});
		}
	};
	accesosTable = createDataTable2('tbl_perfil',accesosOptions);

	var successAccesos = function(){
		$.unblockUI({
		    onUnblock: function(){

		    	$("#modal-perfil-usuario").modal('hide');
		    	accesosTable.fnClearTable();
		    	
				bootbox.dialog({
					message: "Actualización correcta", 
					buttons: {
						"success" : {
						"label" : "OK",
						"className" : "btn-sm btn-primary"
						}
					}
				});
				//reloadPerfil(id_usuarioreg);
			} 
        });
	}

	var errorAccesos = function(){
		$.unblockUI({
		    onUnblock: function(){
				bootbox.dialog({
					message: "Error al intentar actualizar accesos", 
					buttons: {
						"success" : {
						"label" : "OK",
						"className" : "btn-sm btn-primary"
						}
					}
				});
			} 
        });			
	}

	$('#btn-guardar-perfil').click(function(event) {
		event.preventDefault();
		var datos = CopyArray(accesosTable.fnGetData(),["id","estado"]);
		if(datos.length!=0){
			var datossend = {
				datos : datos,
				usuario: id_usuarioreg			
			}
			$.blockUI({
				onBlock: function(){
					enviar(base_url+"usuario/actualizar_perfil",{formulario:datossend}, successAccesos, errorAccesos);
				}
			});
		}
	});

	$('#btn-cancelar-perfil').click(function(event) {
		event.preventDefault();
		$("#modal-perfil-usuario").modal('hide');
		accesosTable.fnClearTable();
	});

});

function reloadTable(){
	usuarioTables.fnReloadAjax(base_url+"servicios/get_usuario_all/");
	cargar_combo_empleado();
}

function cargar_combo_empleado(){
	 $('#empleado-usuario').text("");
	 $.ajax({
        type: "GET",
        url: base_url+"servicios/get_empleado_all/",
        data: {},
        dataType: "json",
        success: function (data) {
        	$('#empleado-usuario').append('<option value="-1">Seleccionar Empleado</option>');
            $.each(data.aaData, function (key,value) {
                $('#empleado-usuario').append('<option data-email="'+value.correo+'" value="'+ value.idempleado+ '">'+value.nombre+" "+value.apellidos+'</option>');
            });
        }
    });
}

function reloadPerfil(id_usuario){
	accesosTable.fnReloadAjax(base_url+"usuario/get_accesos_byperfil/"+id_usuario);
	$("#modal-perfil-usuario").modal('show');
}